#pragma once

#include "cinder/Vector.h"
#include <vector>
#include "cinder/gl/gl.h"
#include "cinder/Rand.h"
#include "cinder/app/AppBasic.h"
#include "cinder/Color.h"
#include "cinder/Timeline.h"

using namespace ci;
using namespace ci::app;

class AttractingParticle {
public:
	AttractingParticle(float canvasWidth, float canvasHeight, Vec2f startingPoint);
	void update(float deltaTime);
	Vec2f getPosition();
	float getSize();
	ColorA getColor();
	bool isDead() { return isDead_; }
	float getHue();
	float getSaturation();
	// test
private:
	Vec2f position;
	Vec2f startingPosition;
	Vec2f targetPosition;
	Vec2f normDirection;
	float speed;
	void reset();
	bool isDead_;
	float startingSize; 
	float currentSize;
	ColorA col;
	float canvasWidth, canvasHeight, canvasHalfWidth, canvasHalfHeight;
	float lifespanLeft;
	float timeUntilIsDying; //start to fade at this point
	float easing;
	bool isDying;
	float theta, curRadius;
	float hue, saturation;
};