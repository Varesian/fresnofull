#pragma once
#include "cinder/Vector.h"
#include "cinder/Rand.h"
#include "cinder/Color.h"
#include "cinder/gl/gl.h"

using namespace ci;
using namespace std;

class Bloomer {
public:
	Bloomer();
	Bloomer(float canvasWidth_, float canvasHeight_);
	void update(float deltaTime);
	void render(int i);
private:
	float lifespanRemaining, totalLifespan, lifespanRatio;
	Vec2f position;
	float canvasWidth, canvasHeight, canvasHalfWidth, canvasHalfHeight;
	void reset();
	float maxSize;
	float speed;
	bool isDead;
	ColorA col;
	float getLifespanRatio();
};
