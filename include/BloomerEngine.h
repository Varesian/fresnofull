#pragma once
#include "Bloomer.h"

class BloomerEngine {
public:
	BloomerEngine();
	BloomerEngine(int canvasWidth, int canvasHeight);
	void update(float deltaTime);
	void draw();
private:
	int canvasWidth, canvasHeight;
	vector<Bloomer> bloomers;
};
