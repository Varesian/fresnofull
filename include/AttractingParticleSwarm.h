//emits particles towards any position given to it beneath a current distance threshold

#pragma once
#include "PixelCoordinate.h"
#include <vector>
#include "cinder/ImageIo.h"
#include "cinder/gl/Texture.h"
#include "cinder/Vector.h"
#include "cinder/gl/gl.h"
#include "cinder/Rand.h"
#include "cinder/app/AppBasic.h"
#include "cinder/Color.h"
#include "cinder/Timeline.h"
#include "AttractingParticle.h"

using namespace ci;
using namespace ci::app;

class AttractingParticleSwarm {
public:
	AttractingParticleSwarm();
	AttractingParticleSwarm(Vec2f position, float distThreshold, cinder::gl::Texture tex, float canvasWidth, float canvasHeight);
	void update(float deltaTime, float numBlobs);
	void draw();
	void addAttractionPoint(Vec2f attractionPoint);
	Vec2f getPosition();
	float getSmoothedNumBlobs();
	float getTimeSinceActivated();
	float getAlphaAttenuation();
private:
	float sqrDistanceThresh;

	//colors to lerp between while emitting
	cinder::ColorA col1;
	cinder::ColorA col2;
	void _init(cinder::gl::Texture tex);
	std::vector<Vec2f> attractionPoints;
	std::vector<AttractingParticle> attractingParticles;

	int maxNumParticles;
	cinder::gl::Texture tex;
	Vec2f position;
	float baseEmissionSpeed; //emission speed with one blob
	float currentEmissionSpeed; //emission speed based on current number of seen blobs
	float minEmissionSpeed; //emission speed can never go below this
	float timeUntilCanEmit;
	float canvasWidth, canvasHeight;
	void updateParticles(float deltaTime);

	bool isActive;
	float activationThresh;
	float timeSinceActivated;
	float smoothedNumBlobs;
	float smoothingParam;

	//this attenuates a particle's alpha
	float alphaAttenuation;

	//for shifting hues
	float hueShift;
	float maxHueShift;
};

