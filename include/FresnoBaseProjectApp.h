#pragma once

 //#define DEV_MODE

#ifdef DEV_MODE
#include "cinder/app/AppNative.h"
#endif

#include <iostream>
#include <fstream>

#include "IEffectInterface.h"

#include "cinder/gl/gl.h"
#include "cinder/ImageIo.h"
#include "cinder/gl/Texture.h"
#include "ParticleEmitter.h"
#include "cinder/Timeline.h"
#include "cinder/gl/TextureFont.h"
#include "rapidjson.h"
#include "document.h"
#include "PixelCoordinate.h"
#include "Umbrella.h"
#include "AttractingParticleSwarm.h"
#include "BloomerEngine.h"

using namespace ci;
using namespace ci::app;
using namespace std;

#ifdef DEV_MODE
class FresnoBaseProjectApp : public AppNative, public IEffectInterface	 {
#else
class FresnoBaseProjectApp : public IEffectInterface	 {
#endif
  public:
	FresnoBaseProjectApp();
	~FresnoBaseProjectApp();
	void setup();
	void mouseDown( MouseEvent event );	
	void update();
	void draw();

	void Update();
	void Draw();

	void Initialize(char* InitData, Timeline* CinderTimeline);
	void SetEffectData(char* Data);
	void InitDevMode();

	int splitPairs(const std::string &s, char delimiter, std::vector<Point> &v);
	void splitPairData(const std::string &s, char delimiter, std::vector<Point> &v);

	std::vector<Point> pixelCoordinates;
	std::vector<Point> umbrellaStemCoordinates;

	bool Rising;
	int CurrentLocation;
	int MoveCounter;

	Vec2f LastGoodCoordinate;

private:

	int counter;
	float redVal, blueVal, greenVal;

	gl::Texture myTexture;	
	float canvasWidth, canvasHeight;
	float canvasHalfWidth, canvasHalfHeight;

	//de-normalize coordinates
	Vec2f getWorldCoords();
	void setCanvasSizeVars(float w, float h);
	ParticleEmitter pe;
	void globalSetup(); //setup common to both dev and production DLL modes	

	Timeline* privateTimeline;// = CinderTimeline;
	float deltaTime;
	float lastRecordedTime;
	void setDeltaTime();

	void drawDebugInfo();
	const std::string CurrentDateTime(void);

	gl::TextureFontRef	mTextureFont;
	Font mFont;

	//multiply screen width times this constant to get screenHeight
	float aspectRatio;

	int frameCount;

	std::vector<Umbrella> umbrellas;

	//for blob tracking
	float param1, param2, param3, param4, param5; 
	std::vector<Point> mLocations;
	std::vector<Point> mDummyLocations;
	std::vector<Point> mSizes;
	void drawDebugBlobs();

	//swallowtails effects
	// AttractingParticleSwarm attractingParticleSwarm;
	std::vector<AttractingParticleSwarm> attractingParticleSwarms;

	//params for activation trigger
	float smoothingParam, triggerThresh;
	std::vector<float> smoothedNumBlobs;
	std::vector<bool> isUmbrellaActive;
	//ofstream blobfile;
	float distanceMultiplier;

	int incomingFrameIndex;

	//color modes for "River"
	//color modes for "River"
	int SceneState;
	int riverColorMode;
	int riverDirection;

	//vars and functions for GUI selection of mode
	int userSelectedMode;
	string userSelectedModeFile;
	void handleUserSelectedMode(string s);
	ColorA selectedUserColor;

	//Bloomer
	BloomerEngine bloomerEngine;

};
