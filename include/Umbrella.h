//do not give the Umbrella class normalized coords to store. It wants world coords.
//The fixture positions are relative to the centroid (local coords in world scale)

#pragma once

#include "cinder/gl/gl.h"
#include "cinder/Utilities.h"
#include "PixelCoordinate.h"
#include "cinder/Rand.h"


#define UMBRELLA_X_SIZE 0.3f
#define UMBRELLA_Y_SIZE 0.4f

using namespace ci;
//using namespace ci::app;

class BloomRingParams {

private:
	float bloomSpeed;
	Color col;

public:
	BloomRingParams() {
		bloomSpeed = 0.5f * Rand::randFloat(0.8f, 1.2f);
		col = Color(Rand::randFloat(0.5f, 1.0f), Rand::randFloat(0.5f, 1.0f), 0.0f);
	}

	float getBloomSpeed() { return bloomSpeed; }
	Color getColor() { return col; }
};

class BloomSinParams {
private:
	float speed;
	Color col;
public:
	BloomSinParams() {
		speed = 0.5f * Rand::randFloat(0.8f, 1.2f);
		col = Color(Rand::randFloat(0.5f, 1.0f), Rand::randFloat(0.5f, 1.0f), 0.0f);
	}

	float getSpeed() { return speed; }
	Color getColor() { return col; }
};

class Umbrella {
public:

	Umbrella(Vec2f upperLeft, std::vector<Point>& pixelCoordinates, Vec2f canvasSize);
	Umbrella();

	Vec2f getCentroid();
	
	Vec2f getFixtureWorldPosition(int index);
	void update(float deltaTime);
	void debugDraw();
	void drawMovingIndex();
	void drawBloom();
	void drawSinBloom();

private:
	Vec2f centroid;
	void spoofFixturePositions();
	std::vector<Vec2f> fixturePositions;

	float radians(float degrees);
	float elapsedTime;
	std::vector<int> rawToSpiralIndexMapper;
	void setSpiralIndexMapper();
	int getRingFromRawIndex(int spiralIndex);
	std::vector<BloomRingParams> bloomRingParams;
	std::vector<BloomSinParams> bloomSinParams;
	float washerSize;
};