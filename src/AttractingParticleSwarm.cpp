#include "AttractingParticleSwarm.h"

//don't use this constructor
AttractingParticleSwarm::AttractingParticleSwarm() {
}

//send in normalized coords
AttractingParticleSwarm::AttractingParticleSwarm(Vec2f position_, float distThreshold_, cinder::gl::Texture tex_, float canvasWidth_, float canvasHeight_):
canvasWidth(canvasWidth_), canvasHeight(canvasHeight_) {
	sqrDistanceThresh = distThreshold_ * distThreshold_;
	position = Vec2f(position_.x, position_.y);
	_init(tex_);
}

void AttractingParticleSwarm::_init(cinder::gl::Texture tex) {
	hueShift = 0;//-60.0f / 360;
	maxHueShift = -50.0f / 360.0f;
	alphaAttenuation = 1.0f; //start off fully attenuated
	col1 = ColorA(1.0f, 1.0f, 0, 0.75f);
	timeSinceActivated = 0;
	maxNumParticles = 50;
	activationThresh = 0.1f;
	this->tex = tex;

	baseEmissionSpeed = .3f;
	currentEmissionSpeed = baseEmissionSpeed;
	minEmissionSpeed = baseEmissionSpeed * 0.2f;
	timeUntilCanEmit = 0;
	isActive = false;

	smoothedNumBlobs = 0;
	smoothingParam = 0.95f;
}

void AttractingParticleSwarm::updateParticles(float deltaTime) {
	int numParticles = attractingParticles.size();
	for (int i = numParticles - 1; i >= 0; i--) {
		if (attractingParticles.size() == 0) break; //sanity check

		attractingParticles[i].update(deltaTime);
		if (attractingParticles[i].isDead()) {
			attractingParticles.erase(attractingParticles.begin() + i);
		}
	}
}

void AttractingParticleSwarm::update(float deltaTime, float numBlobs) {
	float logNumBlobs = log(numBlobs + 1);
	smoothedNumBlobs = smoothingParam * smoothedNumBlobs + (1.0f - smoothingParam) * logNumBlobs;
	if (smoothedNumBlobs > 20) smoothedNumBlobs = 20;

	isActive = smoothedNumBlobs > activationThresh ? true : false;

	if (isActive) {
		timeSinceActivated += deltaTime;
	} else {
		timeSinceActivated = 0;
	}

	float hueShiftSpeed = 0.5f;
	hueShift = -cosf(timeSinceActivated * hueShiftSpeed);
	hueShift = lmap(hueShift, -1.0f, 1.0f, 0.0f, 1.0f);
	hueShift *= maxHueShift;

	float alphaAttenuationSpeed = 0.5f;
	if (isActive) {
		alphaAttenuation += alphaAttenuationSpeed * deltaTime;
	} else {
		alphaAttenuation -= alphaAttenuationSpeed * deltaTime;
	}
	if (alphaAttenuation < 0) {
		alphaAttenuation = 0;
	} else if (alphaAttenuation > 1.0f) {
		alphaAttenuation = 1.0f;
	}

	//reset alpha attentuation
	if (attractingParticles.size() == 0) {
		alphaAttenuation = 1.0f;
	}

	timeUntilCanEmit -= deltaTime;
	if (timeUntilCanEmit <= -1) timeUntilCanEmit = -1;

	attractionPoints.clear();

	updateParticles(deltaTime);

	//handle attraction points
	//pick one attraction point at random and emit
	//int selectedBlob = randInt(attractionPoints.size());
	if (timeUntilCanEmit <= 0 && isActive) {
		timeUntilCanEmit = currentEmissionSpeed * randFloat(0.8f, 1.2f);
		AttractingParticle ap = AttractingParticle(canvasWidth, canvasHeight, position);
		if (attractingParticles.size() < maxNumParticles) {
			attractingParticles.push_back(ap);
		}
	}

}

void AttractingParticleSwarm::draw() {
	tex.enableAndBind();
	int size = attractingParticles.size();

	for (int i = 0; i < size; i++) {
		float h = attractingParticles[i].getHue();
		float sat = attractingParticles[i].getSaturation();
		float alpha = attractingParticles[i].getColor().a;
		ColorA shiftedHueColor = ColorA(CM_HSV, h + hueShift, sat, 1.0f);
		ColorA attenuatedColor = ColorA(shiftedHueColor.r, shiftedHueColor.g, shiftedHueColor.b, alpha * alphaAttenuation);
		gl::color( attenuatedColor );
		float partSize = attractingParticles[i].getSize();
		Vec2f partPos = attractingParticles[i].getPosition();
		//ci::gl::drawSolidCircle(Vec2f(partPos.x, partPos.y), partSize);
		float halfSize = partSize * 0.5f;
		ci::gl::drawSolidRect(Rectf(
			partPos.x - halfSize, partPos.y - halfSize, 
			partPos.x + halfSize, partPos.y + halfSize));
	}
	tex.disable();
}

void AttractingParticleSwarm::addAttractionPoint(Vec2f attractionPoint) {
}

Vec2f AttractingParticleSwarm::getPosition() {
	return position;
}

float AttractingParticleSwarm::getSmoothedNumBlobs() {
	return smoothedNumBlobs;
}

float AttractingParticleSwarm::getTimeSinceActivated() {
	return timeSinceActivated;
}

float AttractingParticleSwarm::getAlphaAttenuation() {
	return alphaAttenuation;
}