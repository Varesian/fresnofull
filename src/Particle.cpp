#include "Particle.h"

Particle::Particle(float canvasWidth_, float canvasHeight_, int colorState_, int direction_) :
	canvasWidth(canvasWidth_), canvasHeight(canvasHeight_), colorState(colorState_), direction(direction_) {
	canvasHalfWidth = canvasWidth * 0.5f;
	canvasHalfHeight = canvasHeight * 0.5f;

	reset();

	//initially set some way back to stagger entrance
	if (direction == 1) {
		position.x = Rand::randFloat(-canvasWidth * 5.0f, -canvasHalfWidth) - size;
	} else {
		position.x = Rand::randFloat(canvasHalfWidth , canvasHalfWidth * 5.0f ) + size;
	}
}

Vec2f Particle::getPosition() {
	return position;
}

float Particle::getSize() {
	return size;
}

ColorA Particle::getColor() {
	return col;
}


void Particle::update(float deltaTime) {
	position += normDirection * speed * deltaTime * direction;

	if (direction == 1) {
		if (position.x > canvasHalfWidth + size) {
			isDead = true;
		}
	} else {
		if (position.x < -canvasHalfWidth - size) {
			isDead = true;
		}
	}

	if (isDead) {
		reset();
	}
}

void Particle::reset() {
	//do not reset color state. Handled upstream

	size = Rand::randFloat(0.07f, 0.12f);
	size *= canvasWidth;

	float x, y;

	if (direction == 1) {
		x = -size - canvasHalfWidth;
	} else {
		x = canvasHalfWidth + size;
	}

	y = Rand::randFloat(-1.0f, 1.0f);
	y *= canvasHalfHeight; 
	position = Vec2f(x, y);
	normDirection = Vec2f(1.0f, 0);
	speed = Rand::randFloat(0.25f, 0.5f);
	speed *= 0.15f;
	speed *= canvasWidth;
	
	stylizeByState();

	isDead = false;
}

void Particle::setColorState(int s) {
	colorState = s;
}

void Particle::stylizeByState() {

	//default
	col = ColorA(Rand::randFloat(0.10f, 0.75f), 1.0f, 1.0f, 0.5f);
	float alpha = 0.5f;

	float r = randFloat(1.0f);

	switch (colorState) {
		case 0:
			//keep default
			break;
		case 1:
			if (r < 0.7f) {
				col = ColorA(randFloat(0, 0.1f), randFloat(0, 0.1f), randFloat(0.7, 1.0f), alpha);
			} else {
				col = ColorA(66.0f / 255, randFloat(230, 244) / 255.0f, randFloat(50, 134) / 255.0f, alpha);
				//size *= .5f;
			}
			break;
		default:
			//keep default
			break;
	}
}