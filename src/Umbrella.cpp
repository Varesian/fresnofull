#include "Umbrella.h"

Umbrella::Umbrella() {
}

Umbrella::Umbrella(Vec2f centroid_, std::vector<Point>& pixelCoordinates_, Vec2f canvasSize_) {
	//centroids are 0 through 1.0 coming from Light Act
	centroid = Vec2f(centroid_.x, centroid_.y);

	/*centroid.x *= canvasSize_.x;
	centroid.y *= canvasSize_.y;*/

	//centroid = Vec2f(upperLeft.x + 0, upperLeft.y + 0);

	elapsedTime = 0;
	setSpiralIndexMapper();
	spoofFixturePositions();

	washerSize = 0.008f * canvasSize_.x;

	bloomRingParams.push_back(BloomRingParams());
	bloomSinParams.push_back(BloomSinParams());
	//bloomRingParams.push_back(BloomRingParams());

	//bloomRingParams.push_back(BloomRingParams());
}

void Umbrella::spoofFixturePositions() {
	float barSpacing = 7.0f;

	float panelAngleOffset = 360 / 6;

	int fixtureCounter = 0;
	for (int panelNum = 0; panelNum < 6; panelNum++) {
		float angleOffset = panelNum * panelAngleOffset;
		float centroidOffset = panelNum * barSpacing;

		for (int barNum = 1; barNum <= 5; barNum++) {
			float ledAngleDiff = 60 / barNum; //how many degrees are between each led fixture
			int numLEDs = barNum;
			for (int ledNum = 1; ledNum <= numLEDs; ledNum++) {
				float theta = radians(angleOffset + -30.0f + ledAngleDiff * ledNum);
				float r = barSpacing * barNum;
				float x = r * cos(theta) + centroid.x;
				float y = r * sin(theta) + centroid.y;
				fixturePositions.push_back(Vec2f(x, y));
			}
		}
	}
}

void Umbrella::update(float deltaTime) {
	elapsedTime += deltaTime;
}

void Umbrella::debugDraw() {
	gl::enableAlphaBlending();
	gl::color(0, 1.0f, 1.0f, 0.2f);
	for (int i = 0; i < fixturePositions.size(); i++) {
		gl::drawSolidCircle(fixturePositions[i], 3);
	}
	gl::color(1.0f, 0, 0, 0.2f);
	gl::drawSolidCircle(centroid, 3);
	gl::disableAlphaBlending();
}

void Umbrella::drawMovingIndex() {
	float movingIndexSpeed = 4.0f;
	float rawSelectedIndex = fmod(elapsedTime * movingIndexSpeed, fixturePositions.size());
	float remainder = rawSelectedIndex - (int) rawSelectedIndex;
	int firstIndex = int(rawSelectedIndex);
	int secondIndex = (firstIndex + 1) % fixturePositions.size();

	//re-map to spiral indices
	firstIndex = rawToSpiralIndexMapper[firstIndex];
	secondIndex = rawToSpiralIndexMapper[secondIndex];

	gl::enableAlphaBlending();
	gl::color(1.0f, 1.0f, 0, 1.0f - remainder);
	gl::drawSolidCircle(fixturePositions[firstIndex], 3);
	gl::color(1.0f, 1.0f, 0, remainder);
	gl::drawSolidCircle(fixturePositions[secondIndex], 3);
	gl::disableAlphaBlending();
}

float Umbrella::radians(float degrees) {
	return (degrees * M_PI) / 180.0f;
}

Vec2f Umbrella::getCentroid() {
	return centroid;
}

void Umbrella::setSpiralIndexMapper() {
	//inner ring
	for (int panelNum = 0; panelNum < 6; panelNum++) {
		rawToSpiralIndexMapper.push_back(panelNum * 15);
	}

	//second ring
	for (int panelNum = 0; panelNum < 6; panelNum++) {
		for (int washerNum = 1; washerNum <= 2; washerNum++) {
			int id = panelNum * 15 + washerNum;
			rawToSpiralIndexMapper.push_back(id);
		}
	}

	//third ring
	for (int panelNum = 0; panelNum < 6; panelNum++) {
		for (int washerNum = 3; washerNum <= 5; washerNum++) {
			int id = panelNum * 15 + washerNum;
			rawToSpiralIndexMapper.push_back(id);
		}
	}

	//fourth ring
	for (int panelNum = 0; panelNum < 6; panelNum++) {
		for (int washerNum = 6; washerNum <= 9; washerNum++) {
			int id = panelNum * 15 + washerNum;
			rawToSpiralIndexMapper.push_back(id);
		}
	}

	//fifth ring
	for (int panelNum = 0; panelNum < 6; panelNum++) {
		for (int washerNum = 10; washerNum <= 14; washerNum++) {
			int id = panelNum * 15 + washerNum;
			rawToSpiralIndexMapper.push_back(id);
		}
	}
}

void Umbrella::drawBloom() {
	for (int bloomerNum = 0; bloomerNum < bloomRingParams.size(); bloomerNum++) {
		float bloomSpeed = bloomRingParams[bloomerNum].getBloomSpeed();
		float rawSelectedRing = fmod(elapsedTime * bloomSpeed, 5);
	
		float remainder = rawSelectedRing - (int) rawSelectedRing;
		int firstRingIndex = int(rawSelectedRing);
		int secondRingIndex = (firstRingIndex + 1) % 5;

		gl::enableAlphaBlending();
	
		for (int i = 0; i < fixturePositions.size(); i++) {
			int ringNum = getRingFromRawIndex(i);
			if (ringNum == firstRingIndex) {
				int spiralIndex = rawToSpiralIndexMapper[i];
				Color col = bloomRingParams[bloomerNum].getColor();
				float alpha = 1.0f - remainder;
				gl::color(col.r, col.g, col.b, alpha);
				float sizeScalar = sin(alpha * M_PI * 0.5f); 
				gl::drawSolidCircle(fixturePositions[spiralIndex], washerSize * sizeScalar);
			} else if (ringNum == secondRingIndex) {
				int spiralIndex = rawToSpiralIndexMapper[i];
				Color col = bloomRingParams[bloomerNum].getColor();
				float alpha = remainder;
				gl::color(col.r, col.g, col.b, alpha);
				float sizeScalar = sin(alpha * M_PI * 0.5f); 
				gl::drawSolidCircle(fixturePositions[spiralIndex], washerSize * alpha * sizeScalar);
			}
		}
		gl::color(1.0f, 0, 0, 0.2f);
		gl::drawSolidCircle(centroid, 3);
		gl::disableAlphaBlending();
	}
}

int Umbrella::getRingFromRawIndex(int rawIndex) {


	if (rawIndex <= 5) {
		return 0;
	} else if (rawIndex <= (5 + 12)) {
		return 1;
	} else if (rawIndex <= (5 + 12 + 18)) {
		return 2;
	} else if (rawIndex <= (5 + 12 + 18 + 24)) {
		return 3;
	} else {
		return 4;
	}
}

void Umbrella::drawSinBloom() {
	for (int bloomerNum = 0; bloomerNum < bloomSinParams.size(); bloomerNum++) {
		float bloomSpeed = bloomSinParams[bloomerNum].getSpeed();

		gl::enableAlphaBlending();
	
		for (int i = 0; i < fixturePositions.size(); i++) {
			int ringNum = getRingFromRawIndex(i);
			float distFromCenter = ringNum;
			float s = sin(distFromCenter - elapsedTime * bloomSpeed);
			float mappedS = lmap(s, -1.0f, 1.0f, 0.0f, 1.0f);
			float alpha = mappedS;
			int spiralIndex = rawToSpiralIndexMapper[i];
			Color c = bloomSinParams[bloomerNum].getColor();
			gl::color(c.r, c.g, c.b, mappedS);
			float sizeScalar = sin(alpha * M_PI * 0.5f); 
			gl::drawSolidCircle(fixturePositions[spiralIndex], washerSize * sizeScalar);
		}
		gl::color(1.0f, 0, 0, 0.2f);
		gl::drawSolidCircle(centroid, 3);
		gl::disableAlphaBlending();
	}
}
