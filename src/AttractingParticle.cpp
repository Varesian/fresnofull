#include "AttractingParticle.h"

AttractingParticle::AttractingParticle(float canvasWidth_, float canvasHeight_, Vec2f startingPoint) 
	: canvasWidth(canvasWidth_), canvasHeight(canvasHeight_) {
	canvasHalfWidth = canvasWidth * 0.5f;
	canvasHalfHeight = canvasHeight * 0.5f;

	startingPosition = Vec2f(startingPoint.x, startingPoint.y);
	
	//targetPosition = Vec2f(attractionPoint.x * canvasWidth, attractionPoint.y * canvasHeight);

	float theta = randFloat(2 * M_PI);
	float radius = 0.3f * canvasWidth_;
	float x = radius * cosf(theta);
	float y = radius * sinf(theta);
	targetPosition = Vec2f(x, y);
	targetPosition += startingPoint;
	reset();

}

Vec2f AttractingParticle::getPosition() {
	return position;
}

float AttractingParticle::getSize() {
	return currentSize;
}

ColorA AttractingParticle::getColor() {
	return col;
}


void AttractingParticle::update(float deltaTime) {

	theta += deltaTime * 1.0f;
	curRadius += deltaTime * 10.0f;
	position.x = curRadius * cosf(theta);
	position.y = curRadius * sinf(theta);
	position += startingPosition;

	lifespanLeft -= deltaTime;
	if (lifespanLeft < timeUntilIsDying) {
		isDying = true;
	}

	if (isDying) {
		float sizeLerp = lmap(lifespanLeft, timeUntilIsDying, 0.0f, 1.0f, 0.0f);
		currentSize = sizeLerp * startingSize;

	}

	if (lifespanLeft <= 0) {
		isDead_ = true;
	}
}

void AttractingParticle::reset() {
	startingSize = 0.08f * canvasWidth;
	currentSize = startingSize;
	position = startingPosition;
	normDirection = Vec2f(targetPosition - startingPosition);
	normDirection.normalize();

	//add some random angle to the normDirection
	float randRads = toRadians(15.0f); //30 degrees randomness


	speed = Rand::randFloat(0.0008f, 0.0012f) * canvasWidth;
	//col = ColorA(1.0f, 1.0f, Rand::randFloat(0.05f, 0.5f), 0.5f);
	hue = float(60) / 360;
	saturation = randFloat(0.5f, 0.95f);
	col = ColorA(CM_HSV, hue, saturation, 1.0f);
	col.a = 0.5f;
	isDead_ = false;
	lifespanLeft = Rand::randFloat(3.0f, 5.0f);
	timeUntilIsDying = lifespanLeft * 0.2f; //is dying for last 10% of life
	easing = 0.15f;
	isDying = false;

	theta = randFloat(0, 2 * M_PI);
	curRadius = 0;
}

float AttractingParticle::getHue() {
	return hue;
}

float AttractingParticle::getSaturation() {
	return saturation;
}
