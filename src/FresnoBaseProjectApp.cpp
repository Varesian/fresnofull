#include "FresnoBaseProjectApp.h"


FresnoBaseProjectApp::FresnoBaseProjectApp() {
	effectID = 53;
}


FresnoBaseProjectApp::~FresnoBaseProjectApp() {
	// blobfile.close();
}

void FresnoBaseProjectApp::InitDevMode() {
	myTexture = loadImage(loadAsset( "dot.png" ));
	setCanvasSizeVars(getWindowSize().x, getWindowSize().y);
}

void FresnoBaseProjectApp::setCanvasSizeVars(float w, float h) {
	canvasWidth = w;
	canvasHeight = h;
	canvasHalfWidth = canvasWidth * 0.5f;
	canvasHalfHeight = canvasHeight * 0.5f;
}

//should only occur in dev (standalone) mode
void FresnoBaseProjectApp::setup()
{
#ifdef DEV_MODE
	InitDevMode();
	globalSetup();
#endif

}

void FresnoBaseProjectApp::mouseDown( MouseEvent event )
{
}

void FresnoBaseProjectApp::update()
{
	Update();
}

void FresnoBaseProjectApp::draw()
{
	Draw();
}

void FresnoBaseProjectApp::Update() {
	frameCount++;
	setDeltaTime();

	for (int i = 0; i<umbrellas.size(); i++) {
		isUmbrellaActive[i] = false;	
		if(smoothedNumBlobs[i] > triggerThresh) {
			isUmbrellaActive[i] = true;	
		}
	}

	bool shouldEmmitNew = false;
	pe.update(deltaTime);
	for(int i=0; i<attractingParticleSwarms.size(); i++) {
		shouldEmmitNew = false;
		if(smoothedNumBlobs[i] > triggerThresh) {
			shouldEmmitNew = true;
		}
		attractingParticleSwarms[i].update(deltaTime, shouldEmmitNew);
	}

	bloomerEngine.update(deltaTime);

	std::vector<Umbrella>::iterator PointIterator = umbrellas.begin();
	while(PointIterator != umbrellas.end()) {
		PointIterator->update(deltaTime);
		++PointIterator;
	}

	MoveCounter++;
	if(MoveCounter > 4) {
		MoveCounter = 0;
		if(Rising) {
			if(CurrentLocation < pixelCoordinates.size()) {
				CurrentLocation++;
			} else {
				Rising = false;
			}
		} else {
			if(CurrentLocation > 0) {
				CurrentLocation--;
			} else {
				Rising = true;
			}
		}
	}

}

void FresnoBaseProjectApp::setDeltaTime() {
#ifdef DEV_MODE
	float curRecordedTime = cinder::app::getElapsedSeconds();
	deltaTime =  curRecordedTime - lastRecordedTime;
	lastRecordedTime = curRecordedTime;
#else
	float curRecordedTime = privateTimeline->getCurrentTime();
	deltaTime = curRecordedTime - lastRecordedTime;
	lastRecordedTime = curRecordedTime;
#endif
}

void FresnoBaseProjectApp::Draw() {
	isLocked = true;

#ifdef DEV_MODE
	gl::pushMatrices();
	gl::translate(canvasHalfWidth, canvasHalfHeight); 
#endif
	// gl::clear( Color( 0, 0, 0 ) ); 

	if (userSelectedMode == 0) {
		gl::enableAlphaBlending();

		float pxSize = 10.0f;
		glColor4f(1.0f, 1.0f, 0.f, 1.0f);
		gl::enableAlphaBlending();

		//draw rectangles at pixel coords
		//for(int i=0; i<pixelCoordinates.size(); i++) {
		//	float drawOffsetX = ((pixelCoordinates[i].x - 0.5f) * float(Width) - pxSize / 2.0f);
		//	float drawOffsetY = ((pixelCoordinates[i].y - 0.5f) * float(Height)  - pxSize / 2.0f);
		//		gl::drawSolidRect(Rectf(drawOffsetX, drawOffsetY, drawOffsetX + pxSize, drawOffsetY + pxSize),true);
		//}

		pe.render(); //river
		//bloomerEngine.draw();

		//draw the swirl when blobs are detected
		for(int i=0; i<attractingParticleSwarms.size(); i++) {
			attractingParticleSwarms[i].draw();
		}

		// drawing washer pixels
		//float pxSize = 8.0f;
		//glColor4f(.0f, 1.0f, 1.f, 1.0f);
		//for(int i=0; i<pixelCoordinates.size(); i++) {
		//	float drawOffsetX = ((pixelCoordinates[i].GetX() - 0.5f) * canvasWidth - pxSize / 2.0f);
		//	float drawOffsetY = ((pixelCoordinates[i].GetY() - 0.5f) * canvasHeight  - pxSize / 2.0f);
		//	if(i == CurrentLocation) {
		//		glColor4f(1.0f, 0.0f, 0.f, 1.0f);
		//	} else {
		//		glColor4f(.0f, 1.0f, .0f, 1.0f);
		//	}
		//	gl::drawSolidRect(Rectf(drawOffsetX, drawOffsetY, drawOffsetX + pxSize, drawOffsetY + pxSize),true);
		//}

		gl::disableAlphaBlending();

		// drawDebugBlobs();

		//std::vector<Umbrella>::iterator PointIterator = umbrellas.begin();
		//while(PointIterator != umbrellas.end()) {
		//	PointIterator->drawSinBloom(); // drawMovingIndex();
		//	++PointIterator;
		//}

		//gl::color(0.0f, 0, 1.0f, 1.0f);
		//for (int i = 0; i < 5; i++) {
		//	// umbrellas[i].debugDraw();
		//	// gl::drawSolidCircle(umbrellas[i].getCentroid(), 0.02 * canvasWidth);
		//	gl::drawSolidCircle(Vec2f( umbrellaStemCoordinates[i].GetX(), umbrellaStemCoordinates[i].GetY()), 0.02 * canvasWidth);
		//}

		//for (int i = 0; i<umbrellas.size(); i++) {
		//	if(smoothedNumBlobs[i] > triggerThresh) {
		//		gl::color(.0f, 1.0f, 0, 1.0f);
		//		// gl::drawSolidCircle(umbrellas[i].getCentroid(), 0.04 * canvasWidth);
		//		//blobfile << "Umbrella " + to_string(i) + ", smoothedNumBlobs[i]: " + to_string(smoothedNumBlobs[i]) << endl;
		//		gl::drawSolidCircle(Vec2f( umbrellaStemCoordinates[i].GetX(), umbrellaStemCoordinates[i].GetY()), 0.04 * canvasWidth);
		//	}
		//}

		// drawing last good config
		/*float drawOffsetX = (LastGoodCoordinate.x * canvasWidth);
		float drawOffsetY = (LastGoodCoordinate.y * canvasWidth);

		gl::color(1.0f, 0, 0, 1.0f);
		gl::drawSolidCircle(Vec2f(drawOffsetX,drawOffsetY), 8.0f, 16);*/

		// LastGoodCoordinate
		// gl::drawStringCentered(to_string(param1), Vec2f(640,50), ci::ColorA(1,0,0), ci::Font("Arial", 30));

	} else if (userSelectedMode == 1) { //Bloom
		gl::enableAlphaBlending();

		float pxSize = 10.0f;
		glColor4f(1.0f, 1.0f, 0.f, 1.0f);
		gl::enableAlphaBlending();

		//draw Bloomer
		bloomerEngine.draw();

		//draw the swirl when blobs are detected
		for(int i = 0; i < attractingParticleSwarms.size(); i++) {
			attractingParticleSwarms[i].draw();
		}

		gl::disableAlphaBlending();

	} else if (userSelectedMode == 3) {
		gl::clear(Color(selectedUserColor.r, selectedUserColor.g, selectedUserColor.b ));
	}
#ifdef DEV_MODE
	gl::popMatrices();
#endif

	//drawDebugInfo();
	isLocked = false;
}

void FresnoBaseProjectApp::globalSetup() {
	//read from user input to see if random mode selection is overwritten
	userSelectedModeFile = "c://selectedMode/selectedMode.txt";
	userSelectedMode = -1;
	fstream myfile;
	myfile.open (userSelectedModeFile);
	if (myfile.is_open()) {
		string line;
		while (getline (myfile, line)) {
			if (line != "") {
				handleUserSelectedMode(line);
			}
		}
		myfile.close();
		ofstream deleter;
		deleter.open(userSelectedModeFile, ios::out | ios::trunc);
		deleter << "";
		deleter.close();
	}

	if (userSelectedMode == -1) {
		Rand::randomize();
		userSelectedMode = (int) Rand::randFloat(0, 2.0f);
	}

	riverColorMode = 1;
	riverDirection = -1; //only set to 1 or -1

	if(!myTexture) return;

	pe.setup(myTexture);
	counter = 0;

	int numParticles = 20;
	for (int i = 0; i < numParticles; i++) {
		Particle p(canvasWidth, canvasHeight, riverColorMode, riverDirection);
		pe.add(p);
	}

	bloomerEngine = BloomerEngine(canvasWidth, canvasHeight);

	mFont = Font( "Times New Roman", 96 / 2 );
	mTextureFont = gl::TextureFont::create( mFont );
	lastRecordedTime = 0;
	deltaTime = 0;


	aspectRatio = (float) canvasHeight / canvasWidth;

	ofstream myFile;
	myFile.open ("D:\\fresnoLogs\\globalSetup.txt");
	myFile << CurrentDateTime() << endl;

	for (int i = 0; i < 5; i++) {
		Vec2f centroid = Vec2f(umbrellaStemCoordinates[i].GetX(), umbrellaStemCoordinates[i].GetY());
		umbrellas.push_back(Umbrella(centroid, pixelCoordinates, Vec2f(canvasWidth, canvasHeight)));
		Vec2f c = umbrellas[i].getCentroid();
		smoothedNumBlobs.push_back(0.0f);
		isUmbrellaActive.push_back(false);
		myFile << "i: " << i << ", c: " << c.x << "," << c.y << endl;
	}

	myFile.close();
	// blobfile.open ("D:\\fresnoLogs\\UmbrellaInteractivity.txt");

	// blobfile << to_string(umbrellaStemCoordinates[3].GetX()) + " " + to_string(umbrellaStemCoordinates[3].GetY()) << endl;

	for(int i=0; i<5; i++) {
		attractingParticleSwarms.push_back(
			AttractingParticleSwarm(
				Vec2f(umbrellaStemCoordinates[i].GetX(), 
				umbrellaStemCoordinates[i].GetY()),
				10.0f, myTexture, canvasWidth, canvasHeight)
			);
	}
//attractingParticleSwarms
//	attractingParticleSwarm = AttractingParticleSwarm(
//		Vec2f(umbrellaStemCoordinates[3].GetX(), 
//		umbrellaStemCoordinates[3].GetY()),
//		10.0f, myTexture, canvasWidth, canvasHeight);


	//mDummyLocations.push_back(Point(.25,.25));

	// smooothing
	smoothingParam = 0.00f; //95f; // 0.95f;
	triggerThresh = .9f;

	
	// blobfile << to_string(umbrellas.size()) + " umbrellas added";

	LastGoodCoordinate = Vec2f(canvasWidth / 2.0f, canvasHeight / 2.0f);
	distanceMultiplier = 0.6f;

}

void FresnoBaseProjectApp::splitPairData(const std::string &s, char delimiter, std::vector<Point> &v) {
	
	stringstream ss;
	ss.str(s);
	string mItem;

	int i=0;
	Point mPoint;

	while(std::getline(ss, mItem, delimiter)) {
		if((i++)==0) {
			mPoint.SetX(atof(mItem.c_str()));
		} else {
			mPoint.SetY(atof(mItem.c_str()));
		}		
	}

	// fill
	v.push_back(mPoint);
}

int FresnoBaseProjectApp::splitPairs(const std::string &s, char delimiter, std::vector<Point> &v) {

	vector<string> myPairs;
	stringstream ss;
	ss.str(s);

	string mItem;

	while(std::getline(ss, mItem, delimiter)) {
		splitPairData(mItem, ',', v);
	}

	return v.size();

} 

void FresnoBaseProjectApp::Initialize(char* InitData, Timeline* CinderTimeline) {

	privateTimeline = CinderTimeline;

	int length=0;
	while (InitData[length++] != '}'){}

	char* JSON = new char[length];
	for (int i = 0; i < length; i++){
		JSON[i] = InitData[i];
	}
	JSON[length] = '\0';

	rapidjson::Document data;
	data.Parse<0>(JSON);

	canvasWidth = (float)data["canvasWidth"].GetDouble();
	canvasHeight = (float)data["canvasHeight"].GetDouble();
	string myTexturePath = (string)data["texturePath"].GetString();
	// string stringCoordinates = (string)data["pixelCoordinates"].GetString();
	string stringUmbrellaStemCoordinates = (string)data["centroidCoordinates"].GetString();


	// splitPairs(stringCoordinates,'!', pixelCoordinates);
	splitPairs(stringUmbrellaStemCoordinates,'!', umbrellaStemCoordinates);

	char* washerCoorinates = "0.203,0.470;0.180,0.465;0.160,0.460;0.138,0.455;0.118,0.450;0.130,0.410;0.153,0.415;0.173,0.420;0.193,0.425;0.185,0.385;0.165,0.380;0.145,0.370;0.158,0.335;0.178,0.340;0.168,0.300;0.160,0.290;0.138,0.290;0.148,0.320;0.135,0.360;0.125,0.320;0.118,0.280;0.095,0.275;0.103,0.315;0.110,0.355;0.120,0.395;0.105,0.430;0.095,0.390;0.088,0.350;0.080,0.310;0.073,0.275;0.073,0.240;0.088,0.205;0.100,0.175;0.113,0.140;0.125,0.105;0.135,0.150;0.123,0.180;0.110,0.215;0.098,0.250;0.120,0.255;0.133,0.225;0.145,0.190;0.155,0.230;0.138,0.265;0.163,0.270;0.173,0.260;0.185,0.225;0.163,0.220;0.155,0.180;0.175,0.185;0.198,0.190;0.210,0.155;0.190,0.150;0.168,0.145;0.148,0.140;0.140,0.095;0.160,0.100;0.180,0.105;0.203,0.110;0.223,0.115;0.238,0.135;0.245,0.175;0.253,0.210;0.263,0.250;0.270,0.290;0.245,0.285;0.238,0.245;0.230,0.210;0.223,0.170;0.208,0.205;0.215,0.245;0.223,0.285;0.203,0.280;0.193,0.240;0.180,0.275;0.180,0.295;0.188,0.335;0.200,0.300;0.223,0.305;0.210,0.340;0.198,0.375;0.208,0.415;0.220,0.380;0.233,0.345;0.245,0.315;0.268,0.320;0.255,0.355;0.243,0.390;0.230,0.425;0.218,0.455;0.270,0.910;0.250,0.910;0.230,0.905;0.208,0.905;0.185,0.905;0.198,0.865;0.220,0.865;0.240,0.865;0.260,0.870;0.250,0.830;0.230,0.825;0.210,0.825;0.220,0.790;0.240,0.790;0.230,0.755;0.223,0.745;0.200,0.745;0.210,0.775;0.198,0.815;0.188,0.780;0.178,0.745;0.155,0.745;0.165,0.780;0.175,0.815;0.185,0.850;0.173,0.885;0.163,0.850;0.153,0.815;0.143,0.780;0.133,0.745;0.133,0.710;0.143,0.675;0.155,0.645;0.165,0.610;0.178,0.575;0.190,0.615;0.178,0.650;0.168,0.680;0.155,0.715;0.180,0.720;0.190,0.685;0.203,0.650;0.213,0.690;0.200,0.725;0.223,0.725;0.233,0.715;0.243,0.680;0.223,0.680;0.213,0.640;0.233,0.640;0.255,0.645;0.265,0.605;0.245,0.605;0.223,0.605;0.203,0.600;0.193,0.560;0.213,0.560;0.235,0.565;0.255,0.565;0.278,0.565;0.293,0.585;0.303,0.620;0.313,0.655;0.323,0.690;0.333,0.725;0.308,0.725;0.298,0.690;0.288,0.655;0.278,0.620;0.265,0.655;0.275,0.690;0.285,0.725;0.263,0.725;0.253,0.690;0.240,0.725;0.240,0.745;0.250,0.780;0.263,0.745;0.285,0.750;0.273,0.785;0.263,0.820;0.275,0.855;0.285,0.820;0.295,0.785;0.308,0.755;0.330,0.760;0.320,0.790;0.310,0.825;0.298,0.860;0.285,0.895;0.505,0.585;0.483,0.580;0.463,0.575;0.440,0.575;0.418,0.570;0.430,0.525;0.453,0.530;0.475,0.535;0.498,0.540;0.488,0.495;0.465,0.490;0.443,0.485;0.455,0.445;0.478,0.450;0.468,0.410;0.460,0.395;0.435,0.395;0.445,0.430;0.433,0.470;0.423,0.430;0.413,0.390;0.390,0.390;0.400,0.430;0.408,0.470;0.418,0.510;0.403,0.550;0.393,0.510;0.383,0.470;0.375,0.425;0.365,0.385;0.368,0.350;0.380,0.315;0.393,0.280;0.405,0.240;0.418,0.205;0.430,0.250;0.418,0.285;0.405,0.320;0.390,0.360;0.415,0.365;0.428,0.330;0.440,0.290;0.450,0.335;0.435,0.375;0.460,0.375;0.470,0.365;0.483,0.330;0.463,0.325;0.453,0.280;0.475,0.285;0.498,0.290;0.510,0.250;0.488,0.245;0.465,0.240;0.443,0.235;0.433,0.190;0.455,0.195;0.478,0.200;0.500,0.200;0.523,0.205;0.538,0.225;0.548,0.270;0.558,0.305;0.565,0.350;0.575,0.390;0.550,0.385;0.540,0.345;0.533,0.305;0.523,0.265;0.508,0.305;0.518,0.345;0.525,0.385;0.503,0.380;0.493,0.340;0.480,0.380;0.480,0.400;0.490,0.440;0.503,0.405;0.525,0.410;0.513,0.445;0.500,0.485;0.510,0.525;0.523,0.490;0.535,0.455;0.550,0.415;0.573,0.425;0.560,0.460;0.548,0.495;0.535,0.535;0.523,0.570;0.800,0.465;0.775,0.460;0.753,0.460;0.730,0.455;0.705,0.455;0.720,0.410;0.743,0.410;0.765,0.415;0.788,0.415;0.778,0.370;0.755,0.370;0.733,0.365;0.745,0.325;0.768,0.325;0.758,0.285;0.748,0.270;0.723,0.275;0.733,0.310;0.720,0.350;0.710,0.310;0.700,0.270;0.675,0.270;0.685,0.310;0.695,0.350;0.705,0.395;0.690,0.435;0.680,0.395;0.670,0.350;0.658,0.310;0.648,0.270;0.650,0.230;0.663,0.195;0.675,0.155;0.688,0.115;0.700,0.075;0.713,0.120;0.700,0.160;0.688,0.200;0.675,0.240;0.700,0.245;0.713,0.205;0.725,0.165;0.738,0.205;0.723,0.250;0.748,0.250;0.758,0.235;0.770,0.200;0.748,0.200;0.738,0.155;0.760,0.155;0.783,0.160;0.795,0.115;0.773,0.115;0.750,0.110;0.728,0.110;0.718,0.060;0.740,0.060;0.763,0.065;0.788,0.065;0.810,0.070;0.825,0.090;0.835,0.130;0.845,0.170;0.858,0.215;0.868,0.255;0.840,0.255;0.830,0.215;0.820,0.175;0.810,0.130;0.795,0.175;0.805,0.215;0.815,0.255;0.793,0.255;0.780,0.215;0.768,0.250;0.768,0.275;0.778,0.315;0.790,0.275;0.815,0.280;0.803,0.320;0.790,0.360;0.803,0.400;0.815,0.365;0.828,0.325;0.840,0.285;0.865,0.290;0.853,0.330;0.840,0.370;0.828,0.410;0.815,0.445;0.848,0.950;0.825,0.940;0.803,0.935;0.780,0.925;0.758,0.920;0.773,0.875;0.798,0.885;0.818,0.890;0.843,0.900;0.835,0.855;0.810,0.845;0.788,0.835;0.803,0.800;0.825,0.805;0.818,0.765;0.808,0.745;0.785,0.745;0.793,0.780;0.778,0.820;0.770,0.775;0.763,0.735;0.738,0.730;0.745,0.770;0.753,0.815;0.763,0.855;0.743,0.895;0.735,0.850;0.728,0.810;0.720,0.765;0.713,0.720;0.715,0.685;0.730,0.650;0.745,0.615;0.760,0.580;0.775,0.545;0.785,0.590;0.770,0.625;0.755,0.660;0.740,0.695;0.765,0.710;0.780,0.670;0.795,0.640;0.803,0.680;0.785,0.720;0.810,0.725;0.823,0.715;0.838,0.685;0.815,0.675;0.808,0.630;0.830,0.635;0.853,0.645;0.868,0.605;0.845,0.595;0.823,0.590;0.800,0.580;0.793,0.530;0.815,0.540;0.838,0.550;0.860,0.555;0.883,0.565;0.898,0.590;0.905,0.630;0.913,0.675;0.920,0.720;0.928,0.760;0.903,0.755;0.895,0.710;0.888,0.670;0.880,0.625;0.863,0.660;0.870,0.705;0.878,0.750;0.855,0.740;0.845,0.700;0.833,0.735;0.830,0.755;0.838,0.800;0.853,0.765;0.875,0.775;0.863,0.810;0.848,0.845;0.855,0.890;0.870,0.855;0.885,0.820;0.900,0.785;0.925,0.795;0.910,0.830;0.895,0.865;0.880,0.900;0.865,0.935;";
	splitPairs(washerCoorinates,';', pixelCoordinates);

	ofstream myfile2;
	myfile2.open ("D:\\fresnoLogs\\stringUmbrellaStemCoordinates.txt");
	myfile2 << stringUmbrellaStemCoordinates << endl;
	

	for(int temp = 0; temp < umbrellaStemCoordinates.size(); temp++) {
		Point p = Point(umbrellaStemCoordinates[temp]);
		umbrellaStemCoordinates[temp] = Point((p.GetX() - .5f) * canvasWidth, (p.GetY() - .5f) * canvasHeight);
		myfile2 << to_string(umbrellaStemCoordinates[temp].GetX()) + "; " + to_string(umbrellaStemCoordinates[temp].GetY()) << endl;
	}

	myfile2.close();

	const char *cstr = myTexturePath.c_str();
	// const char *cstr2 = stringCoordinates.c_str();

	std::stringstream ss;
	std::string s;
	ss << std::noskipws << myTexturePath;
	s = ss.str();

	std::ifstream infile(cstr);

	if(infile.good()){
		myTexture = gl::Texture(loadImage(s.c_str()));					
	} else {
		myTexture = gl::Texture(loadImage("DefaultAssets/images/dot.png"));
	}

	setCanvasSizeVars(canvasWidth, canvasHeight);	


	//pixel coordinates
	//char* washerCoorinates = "0.203,0.470;0.180,0.465;0.160,0.460;0.138,0.455;0.118,0.450;0.130,0.410;0.153,0.415;0.173,0.420;0.193,0.425;0.185,0.385;0.165,0.380;0.145,0.370;0.158,0.335;0.178,0.340;0.168,0.300;0.160,0.290;0.138,0.290;0.148,0.320;0.135,0.360;0.125,0.320;0.118,0.280;0.095,0.275;0.103,0.315;0.110,0.355;0.120,0.395;0.105,0.430;0.095,0.390;0.088,0.350;0.080,0.310;0.073,0.275;0.073,0.240;0.088,0.205;0.100,0.175;0.113,0.140;0.125,0.105;0.135,0.150;0.123,0.180;0.110,0.215;0.098,0.250;0.120,0.255;0.133,0.225;0.145,0.190;0.155,0.230;0.138,0.265;0.163,0.270;0.173,0.260;0.185,0.225;0.163,0.220;0.155,0.180;0.175,0.185;0.198,0.190;0.210,0.155;0.190,0.150;0.168,0.145;0.148,0.140;0.140,0.095;0.160,0.100;0.180,0.105;0.203,0.110;0.223,0.115;0.238,0.135;0.245,0.175;0.253,0.210;0.263,0.250;0.270,0.290;0.245,0.285;0.238,0.245;0.230,0.210;0.223,0.170;0.208,0.205;0.215,0.245;0.223,0.285;0.203,0.280;0.193,0.240;0.180,0.275;0.180,0.295;0.188,0.335;0.200,0.300;0.223,0.305;0.210,0.340;0.198,0.375;0.208,0.415;0.220,0.380;0.233,0.345;0.245,0.315;0.268,0.320;0.255,0.355;0.243,0.390;0.230,0.425;0.218,0.455;0.270,0.910;0.250,0.910;0.230,0.905;0.208,0.905;0.185,0.905;0.198,0.865;0.220,0.865;0.240,0.865;0.260,0.870;0.250,0.830;0.230,0.825;0.210,0.825;0.220,0.790;0.240,0.790;0.230,0.755;0.223,0.745;0.200,0.745;0.210,0.775;0.198,0.815;0.188,0.780;0.178,0.745;0.155,0.745;0.165,0.780;0.175,0.815;0.185,0.850;0.173,0.885;0.163,0.850;0.153,0.815;0.143,0.780;0.133,0.745;0.133,0.710;0.143,0.675;0.155,0.645;0.165,0.610;0.178,0.575;0.190,0.615;0.178,0.650;0.168,0.680;0.155,0.715;0.180,0.720;0.190,0.685;0.203,0.650;0.213,0.690;0.200,0.725;0.223,0.725;0.233,0.715;0.243,0.680;0.223,0.680;0.213,0.640;0.233,0.640;0.255,0.645;0.265,0.605;0.245,0.605;0.223,0.605;0.203,0.600;0.193,0.560;0.213,0.560;0.235,0.565;0.255,0.565;0.278,0.565;0.293,0.585;0.303,0.620;0.313,0.655;0.323,0.690;0.333,0.725;0.308,0.725;0.298,0.690;0.288,0.655;0.278,0.620;0.265,0.655;0.275,0.690;0.285,0.725;0.263,0.725;0.253,0.690;0.240,0.725;0.240,0.745;0.250,0.780;0.263,0.745;0.285,0.750;0.273,0.785;0.263,0.820;0.275,0.855;0.285,0.820;0.295,0.785;0.308,0.755;0.330,0.760;0.320,0.790;0.310,0.825;0.298,0.860;0.285,0.895;0.505,0.585;0.483,0.580;0.463,0.575;0.440,0.575;0.418,0.570;0.430,0.525;0.453,0.530;0.475,0.535;0.498,0.540;0.488,0.495;0.465,0.490;0.443,0.485;0.455,0.445;0.478,0.450;0.468,0.410;0.460,0.395;0.435,0.395;0.445,0.430;0.433,0.470;0.423,0.430;0.413,0.390;0.390,0.390;0.400,0.430;0.408,0.470;0.418,0.510;0.403,0.550;0.393,0.510;0.383,0.470;0.375,0.425;0.365,0.385;0.368,0.350;0.380,0.315;0.393,0.280;0.405,0.240;0.418,0.205;0.430,0.250;0.418,0.285;0.405,0.320;0.390,0.360;0.415,0.365;0.428,0.330;0.440,0.290;0.450,0.335;0.435,0.375;0.460,0.375;0.470,0.365;0.483,0.330;0.463,0.325;0.453,0.280;0.475,0.285;0.498,0.290;0.510,0.250;0.488,0.245;0.465,0.240;0.443,0.235;0.433,0.190;0.455,0.195;0.478,0.200;0.500,0.200;0.523,0.205;0.538,0.225;0.548,0.270;0.558,0.305;0.565,0.350;0.575,0.390;0.550,0.385;0.540,0.345;0.533,0.305;0.523,0.265;0.508,0.305;0.518,0.345;0.525,0.385;0.503,0.380;0.493,0.340;0.480,0.380;0.480,0.400;0.490,0.440;0.503,0.405;0.525,0.410;0.513,0.445;0.500,0.485;0.510,0.525;0.523,0.490;0.535,0.455;0.550,0.415;0.573,0.425;0.560,0.460;0.548,0.495;0.535,0.535;0.523,0.570;0.800,0.465;0.775,0.460;0.753,0.460;0.730,0.455;0.705,0.455;0.720,0.410;0.743,0.410;0.765,0.415;0.788,0.415;0.778,0.370;0.755,0.370;0.733,0.365;0.745,0.325;0.768,0.325;0.758,0.285;0.748,0.270;0.723,0.275;0.733,0.310;0.720,0.350;0.710,0.310;0.700,0.270;0.675,0.270;0.685,0.310;0.695,0.350;0.705,0.395;0.690,0.435;0.680,0.395;0.670,0.350;0.658,0.310;0.648,0.270;0.650,0.230;0.663,0.195;0.675,0.155;0.688,0.115;0.700,0.075;0.713,0.120;0.700,0.160;0.688,0.200;0.675,0.240;0.700,0.245;0.713,0.205;0.725,0.165;0.738,0.205;0.723,0.250;0.748,0.250;0.758,0.235;0.770,0.200;0.748,0.200;0.738,0.155;0.760,0.155;0.783,0.160;0.795,0.115;0.773,0.115;0.750,0.110;0.728,0.110;0.718,0.060;0.740,0.060;0.763,0.065;0.788,0.065;0.810,0.070;0.825,0.090;0.835,0.130;0.845,0.170;0.858,0.215;0.868,0.255;0.840,0.255;0.830,0.215;0.820,0.175;0.810,0.130;0.795,0.175;0.805,0.215;0.815,0.255;0.793,0.255;0.780,0.215;0.768,0.250;0.768,0.275;0.778,0.315;0.790,0.275;0.815,0.280;0.803,0.320;0.790,0.360;0.803,0.400;0.815,0.365;0.828,0.325;0.840,0.285;0.865,0.290;0.853,0.330;0.840,0.370;0.828,0.410;0.815,0.445;0.848,0.950;0.825,0.940;0.803,0.935;0.780,0.925;0.758,0.920;0.773,0.875;0.798,0.885;0.818,0.890;0.843,0.900;0.835,0.855;0.810,0.845;0.788,0.835;0.803,0.800;0.825,0.805;0.818,0.765;0.808,0.745;0.785,0.745;0.793,0.780;0.778,0.820;0.770,0.775;0.763,0.735;0.738,0.730;0.745,0.770;0.753,0.815;0.763,0.855;0.743,0.895;0.735,0.850;0.728,0.810;0.720,0.765;0.713,0.720;0.715,0.685;0.730,0.650;0.745,0.615;0.760,0.580;0.775,0.545;0.785,0.590;0.770,0.625;0.755,0.660;0.740,0.695;0.765,0.710;0.780,0.670;0.795,0.640;0.803,0.680;0.785,0.720;0.810,0.725;0.823,0.715;0.838,0.685;0.815,0.675;0.808,0.630;0.830,0.635;0.853,0.645;0.868,0.605;0.845,0.595;0.823,0.590;0.800,0.580;0.793,0.530;0.815,0.540;0.838,0.550;0.860,0.555;0.883,0.565;0.898,0.590;0.905,0.630;0.913,0.675;0.920,0.720;0.928,0.760;0.903,0.755;0.895,0.710;0.888,0.670;0.880,0.625;0.863,0.660;0.870,0.705;0.878,0.750;0.855,0.740;0.845,0.700;0.833,0.735;0.830,0.755;0.838,0.800;0.853,0.765;0.875,0.775;0.863,0.810;0.848,0.845;0.855,0.890;0.870,0.855;0.885,0.820;0.900,0.785;0.925,0.795;0.910,0.830;0.895,0.865;0.880,0.900;0.865,0.935;";
	//splitPairs(washerCoorinates,';', pixelCoordinates);

	globalSetup();

	ofstream myfile;
	myfile.open ("D:\\fresnoLogs\\Initialize.txt");
	myfile << CurrentDateTime();
	myfile.close();

	incomingFrameIndex = 0;

	MoveCounter = 0;
	CurrentLocation = 0;
	Rising = true;
}

void FresnoBaseProjectApp::SetEffectData(char* Data) {

	char delimiter = ';';
	char divider = ',';
	int i = 0;
	int j = 0;

	char r[32]; // float 1 ...
	i = j;
	while (Data[i] != divider){
		r[i - j] = Data[i];
		i++;
		}
	r[i - j] = '\0';
	i++;
	j = i;

	char g[32]; // float 2 ...
	i = j;
	while (Data[i] != divider){
		g[i - j] = Data[i];
		i++;
		}
	g[i - j] = '\0';
	i++;
	j = i;

	char b[32]; // float 3 ...
	i = j;
	while (Data[i] != divider){
		b[i - j] = Data[i];
		i++;
		}
	b[i - j] = '\0';
	i++;
	j = i;

	char opacity[32]; // float 4 ...
	i = j;
	while (Data[i] != divider){
		opacity[i - j] = Data[i];
		i++;
		}
	opacity[i - j] = '\0';
	i++;
	j = i;

	char radius[32]; // float 5 ...
	i = j;
	while (Data[i] != divider){
		radius[i - j] = Data[i];
		i++;
		}
	radius[i - j] = '\0';
	i++;
	j = i;

	char elements[32];
	i = j;
	while (Data[i] != divider){
		elements[i - j] = Data[i];
		i++;
		}
	elements[i - j] = '\0';
	i++;
	j = i;

	int num = (int)atof(elements);

	param1 = atof(r);
	param2 = atof(g);
	param3 = atof(b);
	param4 = atof(opacity);
	param5 = atof(radius);

	distanceMultiplier = param1;

	/*if(num > 0) {

		mSizes.clear();
		mLocations.clear();

	}*/

	incomingFrameIndex++;

	for (int element = 0; element < num; element++) {
		
		char coordinatex[32]; // X
		i = j;
		while (Data[i] != divider) {
			coordinatex[i - j] = Data[i];
			i++;
			}
		coordinatex[i - j] = '\0';
		i++;
		j = i;

		char coordinatey[32]; // Y
		i = j;
		while (Data[i] != divider){
			coordinatey[i - j] = Data[i];
			i++;
			}
		coordinatey[i - j] = '\0';
		i++;
		j = i;

		char sizex[32]; // Width (percent)
		i = j;
		while (Data[i] != divider){
			sizex[i - j] = Data[i];
			i++;
			}
		sizex[i - j] = '\0';
		i++;
		j = i;

		char sizey[32]; // Height (percent)
		i = j;
		while (Data[i] != divider){
			sizey[i - j] = Data[i];
			i++;
			}
		sizey[i - j] = '\0';
		i++;
		j = i;

		// center locations
		mDummyLocations.push_back(
			Point(
			((double)atof(coordinatex) - 0.5f) * canvasWidth, 
			((double)atof(coordinatey) - 0.5f) * canvasHeight));

		LastGoodCoordinate = Vec2f(atof(coordinatex) - 0.5f, atof(coordinatey) - 0.5f);
		// blobfile << to_string(atof(coordinatex) - 0.5f) + ", " + to_string(atof(coordinatey) - 0.5f) << endl;

		// size pairs
		mSizes.push_back(
			Point(
			(double)atof(sizex), 
			(double)atof(sizey)));
	}

	if(incomingFrameIndex > 7) {
		incomingFrameIndex = 0;
		mLocations.clear();
	}

	if (mDummyLocations.size() > 0) {
		mLocations.clear();
		for(int dum = 0; dum < mDummyLocations.size(); dum++) {
			mLocations.push_back(Point(mDummyLocations[dum].GetX(),mDummyLocations[dum].GetY()));
		}
		incomingFrameIndex = 0;
	}

	mDummyLocations.clear();

	/*if(mLocations.size()> 0) {
		blobfile << to_string(umbrellas.size()) + " umbrellas used";
	}*/

	float distanceThreshold = distanceMultiplier * canvasWidth * canvasWidth;
	// blobfile << "distanceThreshold = " + to_string(distanceThreshold) + "  distanceMultiplier = " + to_string(distanceMultiplier) << endl;
	// distanceThreshold = distanceThreshold * distanceThreshold;

	int maxBlobsToCheck = 1000;
	for (int i = 0; i<umbrellas.size(); i++) {
		int numBlobsForUmbrella = 0;
		
		for (int posi = 0; posi<mLocations.size(); posi++) {
			if(posi > maxBlobsToCheck) continue;

			Vec2f toTarget = umbrellas[i].getCentroid() - Vec2f(mLocations[posi].GetX(),mLocations[posi].GetY()); 
			float len = toTarget.lengthSquared();
			/*if(i == 0) {
				blobfile << "Umbrella " + to_string(i) + 
					" toTarget.lengthSquared(): " + to_string(len) +
					to_string(umbrellas[i].getCentroid().x) + ", " + to_string(umbrellas[i].getCentroid().y) +
					to_string(mLocations[posi].GetX()) + ", " + to_string(mLocations[posi].GetY()) 	<< endl;
			}*/
			// blobfile << "Umbrella " + to_string(i) + " got hit. toTarget.lengthSquared(): " + to_string(len) << endl;
			if(distanceThreshold > len) {
				numBlobsForUmbrella++;				
			} else {
				
			}

		}

		if(numBlobsForUmbrella > 0) {
			// blobfile << "Umbrella " + to_string(i) + " smoothedNumBlobs[i]: " + to_string(smoothedNumBlobs[i]) << endl;
		}

		smoothedNumBlobs[i] = numBlobsForUmbrella;
		// = smoothingParam * smoothedNumBlobs[i] + (1.0f - smoothingParam) * numBlobsForUmbrella; //  * log(numBlobsForUmbrella + 1);
		/*if(i == 1) {
			blobfile << "smoothedNumBlobs[1]: " + to_string(smoothedNumBlobs[i]) << endl;
		}*/

		if(smoothedNumBlobs[i] > 20.0f) {
			smoothedNumBlobs[i] = 20.0f;
		}
	}

	//for (int i = 0; i<umbrellas.size(); i++) {
	//	smoothedNumBlobs = smoothingParam * smoothedNumBlobs + 
	//		(1.0f - smoothingParam) * log(mLocations.size() + 1);
	//}

	// blobfile << to_string(smoothedNumBlobs[2]) + " " + to_string(smoothedNumBlobs) << endl;

	// myLog << to_string(mLocations.size()).c_str();
	// myLog << to_string(smoothedNumBlobs).c_str();

}

Vec2f getWorldCoords(Vec2f normalizedCoords) {
	Vec2f worldCoords;
#ifdef DEV_MODE

#else
#endif

	return worldCoords;
}

void FresnoBaseProjectApp::drawDebugInfo() {
	gl::enableAlphaBlending();
	gl::color( ColorA( 1, 0.0f, 0.0f, 1.0f ) );
	Rectf boundsRect(0, 
			0, 
			500, 
			200 );
	std::string str;
	str = "p: ";
	str += ci::toString(pixelCoordinates.size());
	mTextureFont->drawString( str, boundsRect );
}

void FresnoBaseProjectApp::drawDebugBlobs() {
	gl::color(1.0, 0, 0, 1.0);

	int mindex = 0;
	std::vector<Point>::iterator PointIterator = mLocations.begin();
	while(PointIterator != mLocations.end()) {
		
		float blibSizeTemp = 32.0f;
		float drawOffsetX = ((PointIterator->GetX()) - blibSizeTemp / 2.0f);
		float drawOffsetY = ((PointIterator->GetY()) - blibSizeTemp / 2.0f);	

		gl::drawSolidRect(Rectf(drawOffsetX, drawOffsetY, drawOffsetX + blibSizeTemp, 
			drawOffsetY + blibSizeTemp));

		mindex++;
		std::advance(PointIterator, 1);
	}
}


void FresnoBaseProjectApp::handleUserSelectedMode(string s) {
	userSelectedMode = stoi(s);
	//str = std::to_string(userSelectedMode);
	//if user sending static color
	if (userSelectedMode == 3) { 
		//std::string s = "split on    whitespace   ";
		std::vector<std::string> result;
		std::istringstream iss(s);
		for(std::string s; iss >> s; ) result.push_back(s);
		float r = stoi(result[1]) / 255.0f;
		float g = stoi(result[2]) / 255.0f;
		float b = stoi(result[3]) / 255.0f;
		selectedUserColor = ColorA(r, g, b, 1.0f);
	}
}

const std::string FresnoBaseProjectApp::CurrentDateTime() {

    time_t     now = time(0);
    struct tm  tstruct;
    char       buf[12];
    tstruct = *localtime(&now);
    strftime(buf, sizeof(buf), "%X", &tstruct);
    return buf;

}

#ifdef DEV_MODE
CINDER_APP_NATIVE( FresnoBaseProjectApp, RendererGl )
#else
extern "C"{
	__declspec(dllexport) IEffectInterface* GetInterface(){
		IEffectInterface * effect = new FresnoBaseProjectApp();
		return effect;
		}}
#endif