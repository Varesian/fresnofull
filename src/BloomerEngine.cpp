#include "BloomerEngine.h"

BloomerEngine::BloomerEngine() {}

BloomerEngine::BloomerEngine(int canvasWidth_, int canvasHeight_) : canvasWidth(canvasWidth_), canvasHeight(canvasHeight_) {
	int numBloomers = 10;
	Rand::randomize();
	for (int i = 0; i < numBloomers; i++) {
		Bloomer b = Bloomer(canvasWidth, canvasHeight);
		bloomers.push_back(b);
	}
}

void BloomerEngine::update(float deltaTime) {
	for (int i = 0; i < bloomers.size(); i++) {
		bloomers[i].update(deltaTime);
	}
}

void BloomerEngine::draw() {
	for (int i = 0; i < bloomers.size(); i++) {
		bloomers[i].render(i);
	}
}