#include "Bloomer.h"

Bloomer::Bloomer() {
}

Bloomer::Bloomer(float canvasWidth_, float canvasHeight_) : canvasWidth(canvasWidth_), canvasHeight(canvasHeight_) {
	canvasHalfHeight = canvasHeight * 0.5f;
	canvasHalfWidth = canvasWidth * 0.5f;
	reset();
}

void Bloomer::update(float deltaTime) {
	lifespanRemaining -= deltaTime;
	lifespanRatio = getLifespanRatio();
	if (lifespanRemaining < 0) reset();
}
void Bloomer::render(int i) {
	//ci::gl::drawSolidRect(ci::Rectf(0, 0, 0, 0));
	gl::pushMatrices();
	gl::enableAlphaBlending();
	float curSize = lifespanRatio * maxSize;
	float curAlpha = (1.0 - lifespanRatio);
	gl::color(ColorA(col.r, col.g, col.b, curAlpha));
	ci::gl::drawSolidCircle(position, curSize);
	gl::disableAlphaBlending();
	gl::popMatrices();
}

void Bloomer::reset() {
	float minLifespan = 1.0f;
	float maxLifespan = 10.0f;
	totalLifespan = Rand::randFloat(minLifespan, maxLifespan);
	totalLifespan = Rand::randFloat(0.0001f, 1.0f);
	totalLifespan = lmap(totalLifespan, 0.0f, 1.0f, 1.0f, 10.0f);

	maxSize = lmap(totalLifespan, minLifespan, maxLifespan, 0.04f, 0.22f);

	float x = Rand::randFloat(-1.0f, 1.0f);
	x *= canvasHalfWidth;

	float y = Rand::randFloat(-1.0f, 1.0f);
	y *= canvasHalfHeight; 
	position = Vec2f(x, y);
	speed = lmap(totalLifespan, minLifespan, maxLifespan, 1.0f, 2.0f); //base speed
	speed *= 0.07f; //scale down

	speed *= canvasWidth;
	maxSize *= canvasWidth;
	lifespanRemaining = totalLifespan;
	lifespanRatio = 0;

	col = ColorA(Rand::randFloat(0.0f, 0.1f), Rand::randFloat(0.2f, 0.9f), Rand::randFloat(0.5f, 1.0f), 1.0f);

	isDead = false;
}

float Bloomer::getLifespanRatio() {
	return 1.0f - lifespanRemaining / totalLifespan;
}